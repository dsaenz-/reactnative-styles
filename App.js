/* eslint-disable comma-dangle */
/* eslint-disable prettier/prettier */

import React from 'react';
import { Text, View, StyleSheet } from 'react-native';

export default function App(){
  return (
    <>
    <View style={estilos.container}>

      <View>
        <Text style={ estilos.titulo } >
            Buscaninos
        </Text>
      </View>

      <Text style={estilos.parrafo}> hola desde app native </Text>

      <View
        style={{ width:50, 
                height:50, 
                backgroundColor: 'darkgoldenrod' 
            }}>
          <Text style={{ color:'white', fontSize:25 }}>3</Text>
      </View>
    <View
        style={{ 
                width:10, 
                height:10, 
                backgroundColor: 'darkgreen' ,
                elevation: 2, //z-index
                position: 'absolute'
            }}
      />

<View
      style={{
              width:100, 
              height:100, 
              backgroundColor: 'crimson' ,
              borderRadius:50,
              elevation: 3, //z-index
              position: 'absolute',
              top:10,
              left:15
          }}
      />
    </View>


    </>
  );
}


const estilos = StyleSheet.create({
  titulo : {
   
    elevation: 10,
    marginTop: 16,
    paddingVertical: 8,
    borderWidth: 4,
    borderColor: "#20232a",
    borderRadius: 6,
    backgroundColor: "#61dafb",
    color: "#20232a",
    textAlign:'center',
    fontSize: 30,
    fontWeight: "bold"
   },

   parrafo:{
     fontSize:13,
     color: 'blue'
   },

   container:{
     marginTop:100,
     marginLeft:20,
     marginRight:20
   }

});